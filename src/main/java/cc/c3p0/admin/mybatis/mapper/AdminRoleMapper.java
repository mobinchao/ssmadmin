package cc.c3p0.admin.mybatis.mapper;

import cc.c3p0.admin.common.mapper.BasicMapper;
import cc.c3p0.admin.mybatis.model.AdminRole;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdminRoleMapper extends BasicMapper<AdminRole> {
    List<AdminRole> selectByIds(String[] split);
}