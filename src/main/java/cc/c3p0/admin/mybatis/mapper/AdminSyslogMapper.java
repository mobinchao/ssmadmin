package cc.c3p0.admin.mybatis.mapper;

import cc.c3p0.admin.common.mapper.BasicMapper;
import cc.c3p0.admin.mybatis.model.AdminSyslog;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminSyslogMapper extends BasicMapper<AdminSyslog> {
}