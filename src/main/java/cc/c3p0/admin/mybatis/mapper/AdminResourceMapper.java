package cc.c3p0.admin.mybatis.mapper;

import cc.c3p0.admin.common.dto.Tree;
import cc.c3p0.admin.common.mapper.BasicMapper;
import cc.c3p0.admin.mybatis.model.AdminResource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdminResourceMapper extends BasicMapper<AdminResource> {

    List<Tree> getSystemIds();

    List<Tree> getResourceByIds(String[] ids);
}