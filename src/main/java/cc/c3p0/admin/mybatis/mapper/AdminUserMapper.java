package cc.c3p0.admin.mybatis.mapper;

import cc.c3p0.admin.common.mapper.BasicMapper;
import cc.c3p0.admin.module.user.vo.UserVO;
import cc.c3p0.admin.mybatis.model.AdminUser;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminUserMapper extends BasicMapper<AdminUser> {

    AdminUser selectUserByUsername(UserVO user);
}