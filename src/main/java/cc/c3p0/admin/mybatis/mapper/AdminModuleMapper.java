package cc.c3p0.admin.mybatis.mapper;

import cc.c3p0.admin.common.dto.Tree;
import cc.c3p0.admin.common.mapper.BasicMapper;
import cc.c3p0.admin.mybatis.model.AdminModule;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdminModuleMapper extends BasicMapper<AdminModule> {
    List<Tree> getModuleTree(String id);
}