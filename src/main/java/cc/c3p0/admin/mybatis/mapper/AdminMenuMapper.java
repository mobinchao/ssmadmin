package cc.c3p0.admin.mybatis.mapper;

import cc.c3p0.admin.common.dto.Tree;
import cc.c3p0.admin.common.mapper.BasicMapper;
import cc.c3p0.admin.mybatis.model.AdminMenu;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface AdminMenuMapper extends BasicMapper<AdminMenu> {

    /**
     * 获取菜单列表
     * @param params
     * @return
     */
    List<AdminMenu> getMenuInfo(Map<String, Object> params);

    /**
     * 获取菜单树
     * @return
     */
    List<Tree> selectMenuTree();
}