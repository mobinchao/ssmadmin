package cc.c3p0.admin.mybatis.model;

import cc.c3p0.admin.common.model.BaseModel;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Table(name = "admin_group")
public class AdminGroup extends BaseModel {
    /**
     * 分组名
     */
    @NotNull(message = "分组名称不能为空！")
    private String name;

    /**
     * 描述
     */
    private String description;

    /**
     * 拥有的角色id
     */
    private String rids;

    /**
     * 获取分组名
     *
     * @return name - 分组名
     */
    public String getName() {
        return name;
    }

    /**
     * 设置分组名
     *
     * @param name 分组名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取描述
     *
     * @return description - 描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置描述
     *
     * @param description 描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public String getRids() {
        return rids;
    }

    public void setRids(String rids) {
        this.rids = rids;
    }
}