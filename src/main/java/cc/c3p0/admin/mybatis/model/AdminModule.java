package cc.c3p0.admin.mybatis.model;

import cc.c3p0.admin.common.model.BaseModel;
import javax.persistence.*;

@Table(name = "admin_module")
public class AdminModule extends BaseModel {
    /**
     * 模块名称
     */
    private String name;

    /**
     * 是否父模块
     */
    private String isparent;

    /**
     * 父级模块id
     */
    private String pmid;

    /**
     * 所属系统id
     */
    private String sid;

    /**
     * 描述
     */
    private String description;

    /**
     * 版本号
     */
    private Long version;

    /**
     * 获取模块名称
     *
     * @return name - 模块名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置模块名称
     *
     * @param name 模块名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取是否父模块
     *
     * @return isparent - 是否父模块
     */
    public String getIsparent() {
        return isparent;
    }

    /**
     * 设置是否父模块
     *
     * @param isparent 是否父模块
     */
    public void setIsparent(String isparent) {
        this.isparent = isparent;
    }

    /**
     * 获取父级模块id
     *
     * @return pmid - 父级模块id
     */
    public String getPmid() {
        return pmid;
    }

    /**
     * 设置父级模块id
     *
     * @param pmid 父级模块id
     */
    public void setPmid(String pmid) {
        this.pmid = pmid;
    }

    /**
     * 获取所属系统id
     *
     * @return sid - 所属系统id
     */
    public String getSid() {
        return sid;
    }

    /**
     * 设置所属系统id
     *
     * @param sid 所属系统id
     */
    public void setSid(String sid) {
        this.sid = sid;
    }

    /**
     * 获取描述
     *
     * @return description - 描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置描述
     *
     * @param description 描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 获取版本号
     *
     * @return version - 版本号
     */
    public Long getVersion() {
        return version;
    }

    /**
     * 设置版本号
     *
     * @param version 版本号
     */
    public void setVersion(Long version) {
        this.version = version;
    }
}