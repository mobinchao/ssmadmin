package cc.c3p0.admin.mybatis.model;

import cc.c3p0.admin.common.model.BaseModel;
import javax.persistence.*;

@Table(name = "admin_resource")
public class AdminResource extends BaseModel {
    /**
     * 功能名称
     */
    private String name;

    /**
     * 功能地址
     */
    private String url;

    /**
     * 是否需要权限验证
     */
    private String privilegess;

    /**
     * 模块id
     */
    private String mid;

    /**
     * 功能描述
     */
    private String description;

    /**
     * 版本号
     */
    private Long version;

    /**
     * 资源类型
     */
    private String type;

    /**
     * 所属系统
     */
    private String sid;

    /**
     * 获取功能名称
     *
     * @return name - 功能名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置功能名称
     *
     * @param name 功能名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取功能地址
     *
     * @return url - 功能地址
     */
    public String getUrl() {
        return url;
    }

    /**
     * 设置功能地址
     *
     * @param url 功能地址
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 获取是否需要权限验证
     *
     * @return privilegess - 是否需要权限验证
     */
    public String getPrivilegess() {
        return privilegess;
    }

    /**
     * 设置是否需要权限验证
     *
     * @param privilegess 是否需要权限验证
     */
    public void setPrivilegess(String privilegess) {
        this.privilegess = privilegess;
    }

    /**
     * 获取模块id
     *
     * @return mid - 模块id
     */
    public String getMid() {
        return mid;
    }

    /**
     * 设置模块id
     *
     * @param mid 模块id
     */
    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 获取版本号
     *
     * @return version - 版本号
     */
    public Long getVersion() {
        return version;
    }

    /**
     * 设置版本号
     *
     * @param version 版本号
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }
}