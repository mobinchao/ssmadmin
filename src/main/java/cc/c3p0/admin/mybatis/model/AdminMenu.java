package cc.c3p0.admin.mybatis.model;

import cc.c3p0.admin.common.model.BaseModel;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Table(name = "admin_menu")
public class AdminMenu extends BaseModel {
    /**
     * 菜单名
     */
    @NotNull(message = "菜单名称不能为空！")
    private String name;

    /**
     * 菜单对应的路径
     */
    private String path;

    /**
     * 父级菜单id
     */
    private String pid;

    /**
     * 所属系统id
     */
    private String sid;

    /**
     * 是否父级菜单
     */
    private Boolean isparent;

    /**
     * 描述
     */
    private String description;

    /**
     * 顺序
     */
    private Integer sort;

    /**
     * 图标
     */
    private String icon;

    /**
     * 获取菜单名
     *
     * @return name - 菜单名
     */
    public String getName() {
        return name;
    }

    /**
     * 设置菜单名
     *
     * @param name 菜单名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取父级菜单id
     *
     * @return pid - 父级菜单id
     */
    public String getPid() {
        return pid;
    }

    /**
     * 设置父级菜单id
     *
     * @param pid 父级菜单id
     */
    public void setPid(String pid) {
        this.pid = pid;
    }

    /**
     * 获取所属系统id
     *
     * @return sid - 所属系统id
     */
    public String getSid() {
        return sid;
    }

    /**
     * 设置所属系统id
     *
     * @param sid 所属系统id
     */
    public void setSid(String sid) {
        this.sid = sid;
    }

    public Boolean getIsparent() {
        return isparent;
    }

    public void setIsparent(Boolean isparent) {
        this.isparent = isparent;
    }

    /**
     * 获取描述
     *
     * @return description - 描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置描述
     *
     * @param description 描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}