package cc.c3p0.admin.mybatis.model;

import cc.c3p0.admin.common.model.BaseModel;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import javax.persistence.*;

@Table(name = "admin_syslog")
public class AdminSyslog extends BaseModel {
    /**
     * action结束时间
     */
    @Column(name = "action_end_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date actionEndTime;

    /**
     * action耗时
     */
    @Column(name = "action_total_time")
    private Long actionTotalTime;

    /**
     * action开始时间
     */
    @Column(name = "action_start_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date actionStartTime;

    /**
     * 结束时间
     */
    @Column(name = "end_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 总耗时
     */
    @Column(name = "total_time")
    private Long totalTime;

    /**
     * 调用方法
     */
    private String method;

    /**
     * 跳转地址
     */
    private String referer;

    /**
     * 开始时间
     */
    @Column(name = "start_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /**
     * 用户agent
     */
    private String useragent;

    /**
     * 资源id
     */
    private String rid;

    private String accept;

    private String acceptencoding;

    private String acceptlanguage;

    /**
     * IP地址
     */
    private String host;

    /**
     * 用户ID
     */
    private String userid;

    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createtime;

    /**
     * 业务处理开始时间
     */
    @Column(name = "service_start_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date serviceStartTime;

    /**
     * 业务处理结束时间
     */
    @Column(name = "service_end_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date serviceEndTime;

    /**
     * 业务处理耗时
     */
    @Column(name = "service_total_time")
    private Long serviceTotalTime;

    /**
     * 日志类型： 0.正常日志，1.异常日志
     */
    private String type;

    /**
     * 客户端cookie
     */
    private byte[] cookie;

    /**
     * 描述
     */
    private String description;

    /**
     * 请求地址
     */
    private String requestpath;

    /**
     * 获取action结束时间
     *
     * @return action_end_time - action结束时间
     */
    public Date getActionEndTime() {
        return actionEndTime;
    }

    /**
     * 设置action结束时间
     *
     * @param actionEndTime action结束时间
     */
    public void setActionEndTime(Date actionEndTime) {
        this.actionEndTime = actionEndTime;
    }

    /**
     * 获取action耗时
     *
     * @return action_total_time - action耗时
     */
    public Long getActionTotalTime() {
        return actionTotalTime;
    }

    /**
     * 设置action耗时
     *
     * @param actionTotalTime action耗时
     */
    public void setActionTotalTime(Long actionTotalTime) {
        this.actionTotalTime = actionTotalTime;
    }

    /**
     * 获取action开始时间
     *
     * @return action_start_time - action开始时间
     */
    public Date getActionStartTime() {
        return actionStartTime;
    }

    /**
     * 设置action开始时间
     *
     * @param actionStartTime action开始时间
     */
    public void setActionStartTime(Date actionStartTime) {
        this.actionStartTime = actionStartTime;
    }

    /**
     * 获取结束时间
     *
     * @return end_time - 结束时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 设置结束时间
     *
     * @param endTime 结束时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取总耗时
     *
     * @return total_time - 总耗时
     */
    public Long getTotalTime() {
        return totalTime;
    }

    /**
     * 设置总耗时
     *
     * @param totalTime 总耗时
     */
    public void setTotalTime(Long totalTime) {
        this.totalTime = totalTime;
    }

    /**
     * 获取调用方法
     *
     * @return method - 调用方法
     */
    public String getMethod() {
        return method;
    }

    /**
     * 设置调用方法
     *
     * @param method 调用方法
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * 获取跳转地址
     *
     * @return referer - 跳转地址
     */
    public String getReferer() {
        return referer;
    }

    /**
     * 设置跳转地址
     *
     * @param referer 跳转地址
     */
    public void setReferer(String referer) {
        this.referer = referer;
    }

    /**
     * 获取开始时间
     *
     * @return start_time - 开始时间
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * 设置开始时间
     *
     * @param startTime 开始时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 获取用户agent
     *
     * @return useragent - 用户agent
     */
    public String getUseragent() {
        return useragent;
    }

    /**
     * 设置用户agent
     *
     * @param useragent 用户agent
     */
    public void setUseragent(String useragent) {
        this.useragent = useragent;
    }

    /**
     * 获取资源id
     *
     * @return rid - 资源id
     */
    public String getRid() {
        return rid;
    }

    /**
     * 设置资源id
     *
     * @param rid 资源id
     */
    public void setRid(String rid) {
        this.rid = rid;
    }

    /**
     * @return accept
     */
    public String getAccept() {
        return accept;
    }

    /**
     * @param accept
     */
    public void setAccept(String accept) {
        this.accept = accept;
    }

    /**
     * @return acceptencoding
     */
    public String getAcceptencoding() {
        return acceptencoding;
    }

    /**
     * @param acceptencoding
     */
    public void setAcceptencoding(String acceptencoding) {
        this.acceptencoding = acceptencoding;
    }

    /**
     * @return acceptlanguage
     */
    public String getAcceptlanguage() {
        return acceptlanguage;
    }

    /**
     * @param acceptlanguage
     */
    public void setAcceptlanguage(String acceptlanguage) {
        this.acceptlanguage = acceptlanguage;
    }

    /**
     * 获取IP地址
     *
     * @return host - IP地址
     */
    public String getHost() {
        return host;
    }

    /**
     * 设置IP地址
     *
     * @param host IP地址
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * 获取用户ID
     *
     * @return userid - 用户ID
     */
    public String getUserid() {
        return userid;
    }

    /**
     * 设置用户ID
     *
     * @param userid 用户ID
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     * @return createtime
     */
    public Date getCreatetime() {
        return createtime;
    }

    /**
     * @param createtime
     */
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    /**
     * 获取业务处理开始时间
     *
     * @return service_start_time - 业务处理开始时间
     */
    public Date getServiceStartTime() {
        return serviceStartTime;
    }

    /**
     * 设置业务处理开始时间
     *
     * @param serviceStartTime 业务处理开始时间
     */
    public void setServiceStartTime(Date serviceStartTime) {
        this.serviceStartTime = serviceStartTime;
    }

    /**
     * 获取业务处理结束时间
     *
     * @return service_end_time - 业务处理结束时间
     */
    public Date getServiceEndTime() {
        return serviceEndTime;
    }

    /**
     * 设置业务处理结束时间
     *
     * @param serviceEndTime 业务处理结束时间
     */
    public void setServiceEndTime(Date serviceEndTime) {
        this.serviceEndTime = serviceEndTime;
    }

    /**
     * 获取业务处理耗时
     *
     * @return service_total_time - 业务处理耗时
     */
    public Long getServiceTotalTime() {
        return serviceTotalTime;
    }

    /**
     * 设置业务处理耗时
     *
     * @param serviceTotalTime 业务处理耗时
     */
    public void setServiceTotalTime(Long serviceTotalTime) {
        this.serviceTotalTime = serviceTotalTime;
    }

    /**
     * 获取日志类型： 0.正常日志，1.异常日志
     *
     * @return type - 日志类型： 0.正常日志，1.异常日志
     */
    public String getType() {
        return type;
    }

    /**
     * 设置日志类型： 0.正常日志，1.异常日志
     *
     * @param type 日志类型： 0.正常日志，1.异常日志
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 获取客户端cookie
     *
     * @return cookie - 客户端cookie
     */
    public byte[] getCookie() {
        return cookie;
    }

    /**
     * 设置客户端cookie
     *
     * @param cookie 客户端cookie
     */
    public void setCookie(byte[] cookie) {
        this.cookie = cookie;
    }

    /**
     * 获取描述
     *
     * @return description - 描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置描述
     *
     * @param description 描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 获取请求地址
     *
     * @return requestpath - 请求地址
     */
    public String getRequestpath() {
        return requestpath;
    }

    /**
     * 设置请求地址
     *
     * @param requestpath 请求地址
     */
    public void setRequestpath(String requestpath) {
        this.requestpath = requestpath;
    }
}