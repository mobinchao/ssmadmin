package cc.c3p0.admin.mybatis.model;

import cc.c3p0.admin.common.model.BaseModel;
import javax.persistence.*;

@Table(name = "admin_role")
public class AdminRole extends BaseModel {
    /**
     * 角色名
     */
    private String name;

    /**
     * 描述
     */
    private String description;

    /**
     * 拥有的菜单
     */
    private String mids;

    /**
     * 用于功能id
     */
    private String rids;

    /**
     * 获取角色名
     *
     * @return name - 角色名
     */
    public String getName() {
        return name;
    }

    /**
     * 设置角色名
     *
     * @param name 角色名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取描述
     *
     * @return description - 描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置描述
     *
     * @param description 描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public String getMids() {
        return mids;
    }

    public void setMids(String mids) {
        this.mids = mids;
    }

    /**
     * 获取用于功能id
     *
     * @return rids - 用于功能id
     */
    public String getRids() {
        return rids;
    }

    /**
     * 设置用于功能id
     *
     * @param rids 用于功能id
     */
    public void setRids(String rids) {
        this.rids = rids;
    }
}