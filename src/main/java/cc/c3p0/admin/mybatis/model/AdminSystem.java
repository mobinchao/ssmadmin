package cc.c3p0.admin.mybatis.model;

import cc.c3p0.admin.common.model.BaseModel;
import javax.persistence.*;

@Table(name = "admin_system")
public class AdminSystem extends BaseModel {
    /**
     * 系统名称
     */
    private String name;

    /**
     * 描述
     */
    private String description;

    /**
     * 数据版本号
     */
    private Long version;

    /**
     * 获取系统名称
     *
     * @return name - 系统名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置系统名称
     *
     * @param name 系统名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取描述
     *
     * @return description - 描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置描述
     *
     * @param description 描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 获取数据版本号
     *
     * @return version - 数据版本号
     */
    public Long getVersion() {
        return version;
    }

    /**
     * 设置数据版本号
     *
     * @param version 数据版本号
     */
    public void setVersion(Long version) {
        this.version = version;
    }
}