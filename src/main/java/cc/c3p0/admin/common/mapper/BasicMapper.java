package cc.c3p0.admin.common.mapper;

import com.github.pagehelper.Page;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

import java.util.List;

/**
 * 自定义Mapper接口
 * Created by binchao on 2017/3/3.
 */
public interface BasicMapper<T> extends Mapper<T>, MySqlMapper<T> {

    Page<T> selectByPage(Object obj);

    List<T> selectList(Object param);
}
