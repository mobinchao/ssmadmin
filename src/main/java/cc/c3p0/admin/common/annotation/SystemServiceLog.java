package cc.c3p0.admin.common.annotation;

import java.lang.annotation.*;

/**
 * 系统业务日志注解
 * Created by binchao on 2017/4/17.
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SystemServiceLog {

    String description() default "";
}
