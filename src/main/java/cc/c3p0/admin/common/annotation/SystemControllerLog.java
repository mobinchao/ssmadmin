package cc.c3p0.admin.common.annotation;

import java.lang.annotation.*;

/**
 * 控制器日志注解
 * Created by binchao on 2017/4/17.
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SystemControllerLog {

    String description () default "";
}
