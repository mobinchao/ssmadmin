package cc.c3p0.admin.common.controller;

import cc.c3p0.admin.common.annotation.SystemControllerLog;
import cc.c3p0.admin.common.dto.Json;
import cc.c3p0.admin.common.dto.QueryParameter;
import cc.c3p0.admin.common.service.BasicServiceI;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import javax.validation.Valid;
import java.util.List;

/**
 * 通用rest控制器
 * Created by binchao on 2017/4/8.
 */
public abstract class BasicRestController<T, E extends QueryParameter> {

    private static final Logger LOG = LoggerFactory
            .getLogger(BasicRestController.class);

    private BasicServiceI<T> basicService;

    protected abstract BasicServiceI<T> getBasicService();

    @RequestMapping("/save")
    @SystemControllerLog
    public Json save(@Valid T t,
            BindingResult result) {
        Json json = new Json();
        if (result.hasErrors()) {
            json.setMsg("保存失败：" + json.handleError(result));
            return json;
        }
        try {
            int i = getBasicService().save(t);
            if (i == 1) {
                json.setMsg("信息保存成功！");
                json.setSuccess(true);
            } else {
                json.setMsg("信息保存失败！");
            }
        } catch (Exception e) {
            LOG.error("信息保存失败：" + e.getMessage(), e);
            json.setMsg("信息保存失败：" + e.getMessage());
        }
        return json;
    }

    @RequestMapping("/delete")
    @SystemControllerLog
    public Json delete(@RequestParam Object id) {
        Json json = new Json("信息删除成功！");
        try {
            int i = getBasicService().delete(id);
            if (i == 1) {
                json.setSuccess(true);
            } else {
                json.setMsg("信息删除失败，信息不存在 ！");
            }
        } catch (Exception e) {
            LOG.error("信息删除失败：" + e.getMessage(), e);
            json.setMsg("信息删除失败：" + e.getMessage());
        }
        return json;
    }

    @RequestMapping("/update")
    @SystemControllerLog
    public Json update(@Valid T t,
            BindingResult result) {
        Json json = new Json();
        if (result.hasErrors()) {
            json.setMsg("信息更新失败：" + json.handleError(result));
            return json;
        }
        try {
            int i = getBasicService().update(t);
            if (i == 1) {
                json.setMsg("信息更新成功！");
                json.setSuccess(true);
            } else {
                json.setMsg("信息更新失败，信息不存在！");
            }
        } catch (Exception e) {
            LOG.error("信息更新失败：" + e.getMessage(), e);
            json.setMsg("信息更新失败：" + e.getMessage());
        }
        return json;
    }

    @RequestMapping("/get")
    @SystemControllerLog
    public Json get(@RequestParam Object id) {
        Json json = new Json("信息获取成功！");
        try {
            T entity = getBasicService().select(id);
            if (null != entity) {
                json.setObj(entity);
                json.setSuccess(true);
            } else {
                json.setMsg("查不到相应信息！");
            }
        } catch (Exception e) {
            LOG.error("信息获取失败：" + e.getMessage(), e);
            json.setMsg("信息获取失败：" + e.getMessage());
        }
        return json;
    }

    /**
     * 获取数据列表并分页
     * @param param
     * @return
     */
    @RequestMapping("/find")
    @SystemControllerLog(description = "查询")
    public Json find(E param) {
      Json json = new Json();
      try {
          Page<T> list = getBasicService().find(param);
          json.setSuccess(true);
          json.setMsg("查询成功！");
          json.setObj(new PageInfo(list));
      } catch (Exception e) {
          LOG.error("查询出错：" + e.getMessage(), e);
          json.setMsg("查询出错：" + e.getMessage());
      }
      return json;
    }

    /**
     * 获取数据列表
     * @param param
     * @return
     */
    @RequestMapping("/findList")
    @SystemControllerLog
    public Json findList(E param) {
        Json json = new Json();
        try {
            List<T> list = getBasicService().findList(param);
            json.setSuccess(true);
            json.setObj(list);
        } catch (Exception e) {
            LOG.error("获取数据列表出错：" + e.getMessage());
            json.setMsg("获取数据列表出错：" + e.getMessage());
        }
        return json;
    }
}
