package cc.c3p0.admin.common.service.impl;

import cc.c3p0.admin.common.annotation.SystemServiceLog;
import cc.c3p0.admin.common.mapper.BasicMapper;
import cc.c3p0.admin.common.service.BasicServiceI;
import com.github.pagehelper.Page;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 通用CRUD业务实现
 * Created by binchao on 2017/4/7.
 */
@Service
public abstract class BasicServiceImpl<T> implements BasicServiceI<T> {

    protected BasicMapper<T> mapper;

    protected abstract BasicMapper<T> getMapper();

    @Override
    @SystemServiceLog
    public int save(T t) {
        return getMapper().insert(t);
    }

    @Override
    @SystemServiceLog
    public int update(T t) {
        return getMapper().updateByPrimaryKeySelective(t);
    }

    @Override
    @SystemServiceLog
    public int delete(Object pk) {
        return getMapper().deleteByPrimaryKey(pk);
    }

    @Override
    @SystemServiceLog
    public T select(Object pk) {
        return getMapper().selectByPrimaryKey(pk);
    }

    @Override
    @SystemServiceLog
    public Page<T> find(Object param) {
        return getMapper().selectByPage(param);
    }

    @Override
    @SystemServiceLog
    public List<T> findList(Object param) {
        return getMapper().selectList(param);
    }

    @Override
    @SystemServiceLog
    public int count(Object o) {
        return getMapper().selectCountByExample(o);
    }
}
