package cc.c3p0.admin.common.service;

import cc.c3p0.admin.common.dto.QueryParameter;
import com.github.pagehelper.Page;

import java.util.List;

/**
 * 通用CRUD业务接口
 * Created by binchao on 2017/4/7.
 */
public interface BasicServiceI<T> {

    int save(T t);

    int update(T t);

    int delete(Object pk);

    T select(Object pk);

    <E extends QueryParameter> Page<T> find(Object param);

    /**
     * 获取数据列表
     * @param param
     * @param <T>
     * @return
     */
    <T> List<T> findList(Object param);

    /**
     * 统计总量
     * @param o
     * @return
     */
    int count(Object o);
}
