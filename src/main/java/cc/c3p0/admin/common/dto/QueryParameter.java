package cc.c3p0.admin.common.dto;
/**
 * 通用查询参数
 * @author binchao
 * 2016年11月9日
 */
public class QueryParameter {
	
	private Integer pageNum = 1;
	
	private Integer pageSize = 10;

	public Integer getPageNum() {
		return pageNum;
	}

	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}


}
