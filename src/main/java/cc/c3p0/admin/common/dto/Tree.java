package cc.c3p0.admin.common.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by binchao on 2017/6/12.
 */
public class Tree implements Serializable {

    private Boolean expand = true;

    private String title;

    private String value;

    private List<Tree> children;

    public Boolean getExpand() {
        return expand;
    }

    public void setExpand(Boolean expand) {
        this.expand = expand;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<Tree> getChildren() {
        return children;
    }

    public void setChildren(List<Tree> children) {
        this.children = children;
    }
}
