package cc.c3p0.admin.common.model;

import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.UUID;

/**
 *  token认证模型
 * Created by binchao on 2017/5/5.
 */
public class TokenModel {

    private String userId;

    private String token;

    private Date timestamp;

    private String host;

    public TokenModel(){}

    public TokenModel(String userId, String token) {
        this.userId = userId;
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getToken() {
        if (StringUtils.isEmpty(token)) {
            return UUID.randomUUID().toString().replace("-", "");
        }
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
