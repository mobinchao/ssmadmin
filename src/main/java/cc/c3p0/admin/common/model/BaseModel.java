package cc.c3p0.admin.common.model;

import org.springframework.util.StringUtils;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.UUID;

/**
 * 基础数据模型
 * Created by binchao on 2017/3/5.
 */
public class BaseModel implements Serializable {

    @Id
    private String id;

    public String getId() {
        if (StringUtils.isEmpty(id)) {
            return UUID.randomUUID().toString().replace("-", "");
        }
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
