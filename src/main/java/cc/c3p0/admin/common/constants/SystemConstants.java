package cc.c3p0.admin.common.constants;

/**
 * 系统常量
 * Created by binchao on 2017/4/17.
 */
public interface SystemConstants {

    /**
     * 系统用户
     */
    String USER = "user";

    Integer TOKEN_EXPIRES_HOUR = 1;

    String AUTHORIZATION = "Authorization";

    String CURRENT_USER_ID = "userId";
}
