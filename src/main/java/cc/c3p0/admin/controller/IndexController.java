package cc.c3p0.admin.controller;

import cc.c3p0.admin.common.dto.Json;
import cc.c3p0.admin.module.user.service.UserServiceI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 首页
 * Created by binchao on 2017/3/2.
 */
@RestController
public class IndexController {

    private static final Logger LOG = LoggerFactory
            .getLogger(IndexController.class);

    @Resource
    private UserServiceI userService;

    @RequestMapping("/")
    public String index() {
        return "Hello Spring MVC!";
    }


    /**
     *
     * @return
     */
    @RequestMapping("/unlogin")
    public Json unLogin() {
        Json json = new Json("用户未登录！");
        json.setCode(-1);
        return json;
    }

    @RequestMapping("/unauth")
    public Json unAuth() {
        Json json = new Json("无权访问！");
        json.setCode(403);
        return json;
    }

    @RequestMapping("/unvalidtoken")
    public Json unValidToken() {
        Json json = new Json("令牌无效！");
        json.setCode(999);
        return json;
    }
}
