package cc.c3p0.admin.controller;

import cc.c3p0.admin.common.annotation.SystemControllerLog;
import cc.c3p0.admin.common.controller.BasicRestController;
import cc.c3p0.admin.common.dto.Json;
import cc.c3p0.admin.common.dto.Tree;
import cc.c3p0.admin.common.service.BasicServiceI;
import cc.c3p0.admin.module.system.dto.SystemQueryParameter;
import cc.c3p0.admin.module.system.service.SystemServiceI;
import cc.c3p0.admin.mybatis.model.AdminSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 系统管理控制器
 * Created by binchao on 2017/4/10.
 */
@RestController
@RequestMapping("/sys/system")
public class SystemController extends BasicRestController<AdminSystem, SystemQueryParameter> {

    private static final Logger LOG = LoggerFactory.getLogger(SystemController.class);

    @Resource
    private SystemServiceI systemService;

    @Override
    protected BasicServiceI<AdminSystem> getBasicService() {
        return systemService;
    }

    @RequestMapping("/getSystemTree")
    @SystemControllerLog
    public Json getSystemTree(@RequestParam String id) {
        Json json = new Json();
        try {
            List<Tree> tree = systemService.getSystemTree(id);
            json.setSuccess(true);
            json.setObj(tree);
        } catch (Exception e) {
            LOG.error("获取系统模块信息出错：" + e.getMessage());
            json.setMsg("获取系统模块信息出错：" + e.getMessage());
        }
        return json;
    }
}
