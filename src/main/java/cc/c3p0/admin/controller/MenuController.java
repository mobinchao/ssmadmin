package cc.c3p0.admin.controller;

import cc.c3p0.admin.common.annotation.CurrentUser;
import cc.c3p0.admin.common.controller.BasicRestController;
import cc.c3p0.admin.common.dto.Json;
import cc.c3p0.admin.common.dto.Tree;
import cc.c3p0.admin.common.service.BasicServiceI;
import cc.c3p0.admin.module.menu.dto.MenuQueryParameter;
import cc.c3p0.admin.module.menu.service.MenuServiceI;
import cc.c3p0.admin.module.menu.vo.MenuTreeVO;
import cc.c3p0.admin.mybatis.model.AdminMenu;
import cc.c3p0.admin.mybatis.model.AdminUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import java.util.List;

/**
 * 菜单管理控制器
 * Created by binchao on 2017/4/10.
 */
@RestController
@RequestMapping("/sys/menu")
public class MenuController extends BasicRestController<AdminMenu, MenuQueryParameter> {

    private static final Logger LOG = LoggerFactory.getLogger(MenuController.class);

    @Resource
    private MenuServiceI menuService;

    @Override
    protected BasicServiceI<AdminMenu> getBasicService() {
        return menuService;
    }

    @RequestMapping("/tree")
    public Json tree(@RequestParam(required = false) String sid) {
        Json json = new Json();
        try {
            List<MenuTreeVO> tree = menuService.getMenuTree(sid);
            json.setObj(tree);
            json.setSuccess(true);
        } catch (Exception e) {
            LOG.error("获取菜单出错：{}", e.getMessage(), e);
            json.setMsg("获取菜单出错：" + e.getMessage());
        }
        return json;
    }

    @RequestMapping("/ctree")
    public Json cTree() {
        Json json = new Json();
        try {
            List<Tree> tree = menuService.getCTree();
            json.setSuccess(true);
            json.setObj(tree);
        } catch (Exception e) {
            LOG.error("获取菜单树信息出错：" + e.getMessage());
            json.setMsg("获取菜单树信息出错：" + e.getMessage());
        }
        return json;
    }
}
