package cc.c3p0.admin.controller;

import cc.c3p0.admin.common.controller.BasicRestController;
import cc.c3p0.admin.common.dto.Json;
import cc.c3p0.admin.common.dto.Tree;
import cc.c3p0.admin.common.service.BasicServiceI;
import cc.c3p0.admin.module.group.dto.GroupQueryParameter;
import cc.c3p0.admin.module.group.service.GroupServiceI;
import cc.c3p0.admin.mybatis.model.AdminGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户组管理控制器
 * Created by binchao on 2017/4/8.
 */
@RestController
@RequestMapping("/sys/group")
public class GroupController extends
        BasicRestController<AdminGroup, GroupQueryParameter> {

    private static final Logger LOG = LoggerFactory
            .getLogger(GroupController.class);

    @Resource
    private GroupServiceI groupService;

    @Override
    protected BasicServiceI<AdminGroup> getBasicService() {
        return groupService;
    }

    @RequestMapping("/grantRole")
    public Json grantRole(@RequestParam String id, @RequestParam String rIds) {
        Json json = new Json("授权角色成功！");
        try {
            groupService.grantRole(id, rIds);
            json.setSuccess(true);
        } catch (Exception e) {
            LOG.error("授权角色失败：" + e.getMessage(), e);
            json.setMsg("授权角色失败：" + e.getMessage());
        }
        return json;
    }

    @RequestMapping("/getGroupTree")
    public Json getGroupTree(@RequestParam String id) {
        Json json = new Json();
        try {
            List<Tree> list = groupService.getGroupTree(id);
            json.setSuccess(true);
            json.setObj(list);
        } catch (Exception e) {
            LOG.error("获取用户组树结构出错：" + e.getMessage());
            json.setMsg("获取用户组树结构出错：" + e.getMessage());
        }
        return json;
    }
}
