package cc.c3p0.admin.controller;

import cc.c3p0.admin.common.controller.BasicRestController;
import cc.c3p0.admin.common.dto.Json;
import cc.c3p0.admin.common.service.BasicServiceI;
import cc.c3p0.admin.module.role.dto.RoleQueryParameter;
import cc.c3p0.admin.mybatis.model.AdminRole;
import cc.c3p0.admin.module.role.service.RoleServiceI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 系统日志管理控制器
 */
@RestController
@RequestMapping("/sys/role")
public class RoleController extends
		BasicRestController<AdminRole, RoleQueryParameter> {
	
	private static final Logger LOG = LoggerFactory
			.getLogger(RoleController.class);

	@Resource
	private RoleServiceI roleService;

	@Override
	protected BasicServiceI<AdminRole> getBasicService() {
		return roleService;
	}

	@RequestMapping("/grantMenus")
	public Json grantMenus(@RequestParam String id, @RequestParam String menuIds) {
		Json json = new Json("授权成功！");
		try {
			roleService.grantMenus(id, menuIds);
			json.setSuccess(true);
		} catch (Exception e) {
			LOG.error("菜单授权失败：" + e.getMessage(), e);
			json.setMsg("菜单授权失败：" + e.getMessage());
		}
		return json;
	}

	@RequestMapping("/grantResource")
	public Json grantResource(@RequestParam String id, @RequestParam String resIds) {
		Json json = new Json("资源授权成功！");
		try {
			roleService.grantResources(id, resIds);
			json.setSuccess(true);
		} catch (Exception e) {
			LOG.error("资源授权失败：" + e.getMessage(), e);
			json.setMsg("资源授权失败：" + e.getMessage());
		}
		return json;
	}
}
