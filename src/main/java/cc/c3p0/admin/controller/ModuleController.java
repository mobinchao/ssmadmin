package cc.c3p0.admin.controller;

import cc.c3p0.admin.common.annotation.SystemControllerLog;
import cc.c3p0.admin.common.controller.BasicRestController;
import cc.c3p0.admin.common.dto.Json;
import cc.c3p0.admin.common.dto.Tree;
import cc.c3p0.admin.common.service.BasicServiceI;
import cc.c3p0.admin.module.module.dto.ModuleQueryParameter;
import cc.c3p0.admin.module.module.service.ModuleServiceI;
import cc.c3p0.admin.mybatis.model.AdminModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 模块管理控制器
 * Created by binchao on 2017/4/10.
 */
@RestController
@RequestMapping("/sys/module")
public class ModuleController extends BasicRestController<AdminModule, ModuleQueryParameter> {

    private static final Logger LOG = LoggerFactory.getLogger(ModuleController.class);

    @Resource
    private ModuleServiceI moduleService;

    @Override
    protected BasicServiceI<AdminModule> getBasicService() {
        return moduleService;
    }

    @RequestMapping("/getModuleTree")
    @SystemControllerLog
    public Json getModuleTree(@RequestParam String id) {
        Json json = new Json();
        try {
            List<Tree> list = moduleService.getModuleTree(id);
            json.setSuccess(true);
            json.setObj(list);
        } catch (Exception e) {
            LOG.error("获取模块树结构出错：" + e.getMessage());
            json.setMsg("获取模块树结构出错：" + e.getMessage());
        }
        return json;
    }
}
