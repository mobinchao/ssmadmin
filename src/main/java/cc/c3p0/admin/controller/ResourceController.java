package cc.c3p0.admin.controller;

import cc.c3p0.admin.common.controller.BasicRestController;
import cc.c3p0.admin.common.dto.Json;
import cc.c3p0.admin.common.dto.Tree;
import cc.c3p0.admin.common.service.BasicServiceI;
import cc.c3p0.admin.module.resouce.dto.ResourceQueryParameter;
import cc.c3p0.admin.mybatis.model.AdminResource;
import cc.c3p0.admin.module.resouce.service.ResourceServiceI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 系统资源控制器
 * Created by binchao on 2017/4/8.
 */
@RestController
@RequestMapping("/sys/resource")
public class ResourceController extends
        BasicRestController<AdminResource, ResourceQueryParameter> {

    private static final Logger LOG = LoggerFactory
            .getLogger(ResourceController.class);

    @Resource
    private ResourceServiceI resourceService;

    @Override
    protected BasicServiceI<AdminResource> getBasicService() {
        return resourceService;
    }

    @RequestMapping("/tree")
    public Json getTree() {
        Json json = new Json();
        try {
            List<Tree> list = resourceService.getTree();
            json.setSuccess(true);
            json.setObj(list);
        } catch (Exception e) {
            LOG.error("查询资源信息出错：" + e.getMessage());
            json.setMsg("查询资源信息出错：" + e.getMessage());
        }
        return json;
    }
}
