package cc.c3p0.admin.controller;

import cc.c3p0.admin.common.annotation.SystemControllerLog;
import cc.c3p0.admin.common.dto.Json;
import cc.c3p0.admin.module.syslog.dto.SyslogQueryParameter;
import cc.c3p0.admin.module.syslog.service.SyslogSercieI;
import cc.c3p0.admin.mybatis.model.AdminSyslog;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/**
 * 系统日志控制器
 * Created by binchao on 2017/4/10.
 */
@RestController
@RequestMapping("/sys/syslog")
public class SyslogController {

    private static final Logger LOG = LoggerFactory.getLogger(SyslogController.class);

    @Resource
    private SyslogSercieI syslogSercie;

    /**
     * 查询日志详情
     * @param id
     * @return
     */
    @RequestMapping("/get")
    @SystemControllerLog
    public Json get(@RequestParam String id) {
        Json json = new Json("日志查询成功！");
        try {
            AdminSyslog log = syslogSercie.select(id);
            if (null != log) {
                json.setSuccess(true);
                json.setObj(log);
            } else {
                json.setMsg("日志不存在！");
            }
        } catch (Exception e) {
            LOG.error("查询日志信息出错：" + e.getMessage(), e);
            json.setMsg("查询日志出错：" + e.getMessage());
        }
        return json;
    }

    /**
     * 查询日志列表信息
     * @param param
     * @return
     */
    @RequestMapping("/find")
    @SystemControllerLog
    public Json find(SyslogQueryParameter param) {
        Json json = new Json("查询日志列表成功！");
        try {
            Page<AdminSyslog> list = syslogSercie.find(param);
            json.setSuccess(true);
            json.setObj(new PageInfo(list));
        } catch (Exception e) {
            LOG.error("查询日志列表失败：" + e.getMessage(), e);
            json.setMsg("查询日志列表失败：" + e.getMessage());
        }
        return json;
    }
}
