package cc.c3p0.admin.module.system.service;

import cc.c3p0.admin.common.dto.Tree;
import cc.c3p0.admin.common.service.BasicServiceI;
import cc.c3p0.admin.mybatis.model.AdminSystem;

import java.util.List;

/**
 * 系统管理业务接口
 * Created by binchao on 2017/4/10.
 */
public interface SystemServiceI extends BasicServiceI<AdminSystem> {

    List<Tree> getSystemTree(String id);
}
