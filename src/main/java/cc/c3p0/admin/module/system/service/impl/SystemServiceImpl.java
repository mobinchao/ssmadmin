package cc.c3p0.admin.module.system.service.impl;

import cc.c3p0.admin.common.dto.Tree;
import cc.c3p0.admin.common.mapper.BasicMapper;
import cc.c3p0.admin.common.service.impl.BasicServiceImpl;
import cc.c3p0.admin.mybatis.mapper.AdminSystemMapper;
import cc.c3p0.admin.mybatis.model.AdminSystem;
import cc.c3p0.admin.module.system.service.SystemServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 系统管理业务实现
 * Created by binchao on 2017/4/10.
 */
@Service
public class SystemServiceImpl extends BasicServiceImpl<AdminSystem>
        implements SystemServiceI{

    @Resource
    private AdminSystemMapper systemMapper;

    @Override
    protected BasicMapper<AdminSystem> getMapper() {
        return systemMapper;
    }

    @Override
    public List<Tree> getSystemTree(String id) {
        List<Tree> tree = systemMapper.getSystemTree(id);
        return tree;
    }
}
