package cc.c3p0.admin.module.system.dto;

import cc.c3p0.admin.common.dto.QueryParameter;

/**
 * 系统查询参数Bean
 * Created by binchao on 2017/4/13.
 */
public class SystemQueryParameter extends QueryParameter {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
