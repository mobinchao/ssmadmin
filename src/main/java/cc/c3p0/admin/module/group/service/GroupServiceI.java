package cc.c3p0.admin.module.group.service;

import cc.c3p0.admin.common.dto.Tree;
import cc.c3p0.admin.common.service.BasicServiceI;
import cc.c3p0.admin.mybatis.model.AdminGroup;

import java.util.List;

/**
 * 用户组管理业务接口
 * Created by binchao on 2017/4/7.
 */
public interface GroupServiceI extends BasicServiceI<AdminGroup> {

    /**
     * 授权角色到分组
     * @param id      分组id
     * @param rIds    角色id集合
     */
    void grantRole(String id, String rIds);

    /**
     * 检测用户权限信息
     * @param gIds  用户组
     * @param path  当前路径
     * @return
     */
    Boolean checkPermision(String gIds, String path);

    List<Tree> getGroupTree(String id);
}
