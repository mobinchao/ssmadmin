package cc.c3p0.admin.module.group.dto;

import cc.c3p0.admin.common.dto.QueryParameter;

/**
 * 分组信息查询参数BEAN
 * Created by binchao on 2017/4/7.
 */
public class GroupQueryParameter extends QueryParameter {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
