package cc.c3p0.admin.module.menu.dto;

import cc.c3p0.admin.common.dto.QueryParameter;

/**
 * 菜单查询参数BEAN
 * Created by binchao on 2017/4/13.
 */
public class MenuQueryParameter extends QueryParameter {

    private String name;

    private String path;

    private Boolean isparent;

    private String sid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Boolean getIsparent() {
        return isparent;
    }

    public void setIsparent(Boolean isparent) {
        this.isparent = isparent;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }
}
