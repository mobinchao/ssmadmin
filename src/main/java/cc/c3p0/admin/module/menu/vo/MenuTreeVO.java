package cc.c3p0.admin.module.menu.vo;

import java.util.List;

/**
 * 菜单树
 * Created by binchao on 2017/4/25.
 */
public class MenuTreeVO {

    private String id;

    private String name;

    private String icon;

    private String path;

    private List<MenuTreeVO> subMenu;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<MenuTreeVO> getSubMenu() {
        return subMenu;
    }

    public void setSubMenu(List<MenuTreeVO> subMenu) {
        this.subMenu = subMenu;
    }
}
