package cc.c3p0.admin.module.menu.service;

import cc.c3p0.admin.common.dto.Tree;
import cc.c3p0.admin.common.service.BasicServiceI;
import cc.c3p0.admin.module.menu.vo.MenuTreeVO;
import cc.c3p0.admin.mybatis.model.AdminMenu;
import cc.c3p0.admin.mybatis.model.AdminUser;

import java.util.List;

/**
 * 菜单管理业务接口
 * Created by binchao on 2017/4/9.
 */
public interface MenuServiceI extends BasicServiceI<AdminMenu> {

    /**
     * 获取菜单树
     * @param sid 所属系统ID
     * @return
     */
    List<MenuTreeVO> getMenuTree(String sid);

    /**
     * 获取checkbox tree
     * @return
     */
    List<Tree> getCTree();
}
