package cc.c3p0.admin.module.menu.vo;

import cc.c3p0.admin.mybatis.model.AdminMenu;

/**
 * 菜单VO
 * Created by binchao on 2017/4/25.
 */
public class MenuVO extends AdminMenu {

    /**
     * 所属系统名称
     */
    private String sname;

    /**
     * 父菜单名称
     */
    private String pname;
    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }
}
