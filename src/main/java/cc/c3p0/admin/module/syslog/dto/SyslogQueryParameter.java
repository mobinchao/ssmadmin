package cc.c3p0.admin.module.syslog.dto;

import cc.c3p0.admin.common.dto.QueryParameter;

import java.util.Date;

/**
 *  系统日志查询参数
 * Created by binchao on 2017/4/10.
 */
public class SyslogQueryParameter extends QueryParameter {

    private String type;

    private String method;

    private String requestpath;

    private String host;

    private String timeBegin;

    private String timeEnd;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getRequestpath() {
        return requestpath;
    }

    public void setRequestpath(String requestpath) {
        this.requestpath = requestpath;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getTimeBegin() {
        return timeBegin;
    }

    public void setTimeBegin(String timeBegin) {
        this.timeBegin = timeBegin;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }
}
