package cc.c3p0.admin.module.syslog.service;

import cc.c3p0.admin.common.service.BasicServiceI;
import cc.c3p0.admin.mybatis.model.AdminSyslog;

/**
 * 系统日志管理业务接口
 * Created by binchao on 2017/4/9.
 */
public interface SyslogSercieI extends BasicServiceI<AdminSyslog> {
}
