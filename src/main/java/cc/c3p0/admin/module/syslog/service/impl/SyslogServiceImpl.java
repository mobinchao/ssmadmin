package cc.c3p0.admin.module.syslog.service.impl;

import cc.c3p0.admin.common.mapper.BasicMapper;
import cc.c3p0.admin.common.service.impl.BasicServiceImpl;
import cc.c3p0.admin.mybatis.mapper.AdminSyslogMapper;
import cc.c3p0.admin.mybatis.model.AdminSyslog;
import cc.c3p0.admin.module.syslog.service.SyslogSercieI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 系统日志业务实现
 * Created by binchao on 2017/4/9.
 */
@Service
public class SyslogServiceImpl extends BasicServiceImpl<AdminSyslog>
        implements SyslogSercieI {

    @Resource
    private AdminSyslogMapper syslogMapper;

    @Override
    protected BasicMapper<AdminSyslog> getMapper() {
        return syslogMapper;
    }
}
