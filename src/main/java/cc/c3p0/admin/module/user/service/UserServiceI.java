package cc.c3p0.admin.module.user.service;


import cc.c3p0.admin.common.service.BasicServiceI;
import cc.c3p0.admin.mybatis.model.AdminUser;
import cc.c3p0.admin.module.user.vo.UserVO;

/**
 * 用户管理业务接口
 */
public interface UserServiceI extends BasicServiceI<AdminUser> {

	/**
	 * 注册用户
	 * @param user  用户信息
	 * @return
	 */
	void saveUser(UserVO user);

	/**
	 * 用户登录
	 * @param user  用户信息
	 */
	AdminUser login(UserVO user);

	/**
	 * 更新密码
	 * @param u    用户信息
	 */
	void updatePassword(UserVO u);

	/**
	 * 用户分组
	 * @param id     用户ID
	 * @param gIds   组别ID集合
	 */
    void grantGroup(String id, String gIds);

}
