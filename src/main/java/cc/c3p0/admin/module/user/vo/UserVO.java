package cc.c3p0.admin.module.user.vo;

import java.util.Date;

import javax.validation.constraints.NotNull;

/**
 * 用户信息
 * @author binchao
 * 2016年10月25日
 */
public class UserVO {
	
	/**
	 * 用户名
	 */
	@NotNull(message = "用户名不能为空")
	private String username;
	
	/**
	 * 密码
	 */
	@NotNull(message = "密码不能为空")
	private String password;
	
	private String newpwd;

	@NotNull(message = "用户名称不能为空")
	private String name;

	/**
	 * email
	 */
	private String email;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getNewpwd() {
		return newpwd;
	}

	public void setNewpwd(String newpwd) {
		this.newpwd = newpwd;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
