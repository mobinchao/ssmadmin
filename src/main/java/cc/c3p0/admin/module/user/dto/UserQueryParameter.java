package cc.c3p0.admin.module.user.dto;
import cc.c3p0.admin.common.dto.QueryParameter;

public class UserQueryParameter extends QueryParameter {

	private String username;

	private String name;

	private String email;

	private String status;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
