package cc.c3p0.admin.module.user.service.impl;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;

import cc.c3p0.admin.common.mapper.BasicMapper;
import cc.c3p0.admin.common.service.impl.BasicServiceImpl;
import cc.c3p0.admin.mybatis.mapper.AdminUserMapper;
import cc.c3p0.admin.mybatis.model.AdminUser;
import cc.c3p0.admin.module.user.vo.UserVO;
import cc.c3p0.admin.module.user.service.UserServiceI;
import cc.c3p0.admin.utils.tools.ToolSecurityPbkdf2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.security.auth.login.AccountNotFoundException;

/**
 * 用户管理业务接口
 * @author binchao
 * 2016年10月21日
 */
@Service
public class UserServiceImpl extends BasicServiceImpl<AdminUser>
		implements UserServiceI {
	
	private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

	@Resource
	private AdminUserMapper userMapper;

	@Override
	protected BasicMapper<AdminUser> getMapper() {
		return userMapper;
	}

	@Override
	public void saveUser(UserVO user) {
		AdminUser u = new AdminUser();
		BeanUtils.copyProperties(user, u);
		u.setStatus("1");
		u.setCreatetime(new Date());
		try {
			byte[] salt = ToolSecurityPbkdf2.generateSalt();
			u.setSalt(salt);
			byte[] password = ToolSecurityPbkdf2.getEncryptedPassword(user.getPassword(), salt);
			u.setPassword(password);
		} catch (NoSuchAlgorithmException e) {
			LOG.error("密码盐生成失败！", e);
			throw new RuntimeException(e);
		} catch (InvalidKeySpecException e) {
			LOG.error("密码加密失败！", e);
			throw new RuntimeException(e);
		}
		try {
			userMapper.insert(u);
		} catch (Exception e) {
			LOG.error("注册新用户失败。", e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public AdminUser login(UserVO user) {
		try {
			AdminUser u = userMapper.selectUserByUsername(user);
			if (null != u) {
				boolean flag = ToolSecurityPbkdf2.authenticate(user.getPassword(), u.getPassword(), u.getSalt());
				if(!flag) {
					throw new RuntimeException("密码不正确！");
				}
				return u;
			} else {
				throw new AccountNotFoundException("账户不存在！");
			}
		} catch (Exception e) {
			LOG.error("登录失败：" + e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public void updatePassword(UserVO user) {
		try {
			AdminUser u = userMapper.selectByPrimaryKey(user);
			boolean flag = ToolSecurityPbkdf2.authenticate(user.getPassword(), u.getPassword(), u.getSalt());
			if(flag) {
				byte[] salt = ToolSecurityPbkdf2.generateSalt();
				u.setSalt(salt);
				byte[] password = ToolSecurityPbkdf2.getEncryptedPassword(user.getNewpwd(), salt);
				u.setPassword(password);
				u.setUpdatetime(new Date());
				userMapper.updateByPrimaryKey(u);
			} else {
				throw new Exception("原密码不正确！");
			}
		} catch (Exception e) {
			LOG.error("密码更新失败：" + e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public void grantGroup(String id, String gIds) {
		AdminUser user = new AdminUser();
		user.setId(id);
		user.setGids(gIds);
		userMapper.updateByPrimaryKeySelective(user);
	}
}
