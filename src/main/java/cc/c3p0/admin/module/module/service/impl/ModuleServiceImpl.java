package cc.c3p0.admin.module.module.service.impl;

import cc.c3p0.admin.common.dto.Tree;
import cc.c3p0.admin.common.mapper.BasicMapper;
import cc.c3p0.admin.common.service.impl.BasicServiceImpl;
import cc.c3p0.admin.module.module.service.ModuleServiceI;
import cc.c3p0.admin.mybatis.mapper.AdminModuleMapper;
import cc.c3p0.admin.mybatis.model.AdminModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 模块管理业务实现
 * Created by binchao on 2017/4/9.
 */
@Service
public class ModuleServiceImpl extends BasicServiceImpl<AdminModule>
        implements ModuleServiceI {

    @Resource
    private AdminModuleMapper moduleMapper;

    @Override
    protected BasicMapper<AdminModule> getMapper() {
        return moduleMapper;
    }

    @Override
    public List<Tree> getModuleTree(String id) {
        List<Tree> list = moduleMapper.getModuleTree(id);
        return list;
    }
}
