package cc.c3p0.admin.module.module.vo;

import cc.c3p0.admin.mybatis.model.AdminModule;

/**
 * 模块VO
 * Created by binchao on 2017/4/21.
 */
public class ModuleVO extends AdminModule {

    private String sname;

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }
}
