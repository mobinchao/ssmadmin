package cc.c3p0.admin.module.module.dto;

import cc.c3p0.admin.common.dto.QueryParameter;

/**
 * 模块查询参数BEAN
 * Created by binchao on 2017/4/13.
 */
public class ModuleQueryParameter extends QueryParameter {

    private String name;

    private String sid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }
}
