package cc.c3p0.admin.module.module.service;

import cc.c3p0.admin.common.dto.Tree;
import cc.c3p0.admin.common.service.BasicServiceI;
import cc.c3p0.admin.mybatis.model.AdminModule;

import java.util.List;

/**
 * 模块管理业务接口
 * Created by binchao on 2017/4/9.
 */
public interface ModuleServiceI extends BasicServiceI<AdminModule> {

    /**
     * 获取模块树信息
     * @param id
     * @return
     */
    List<Tree> getModuleTree(String id);
}
