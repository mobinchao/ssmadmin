package cc.c3p0.admin.module.role.dto;

import cc.c3p0.admin.common.dto.QueryParameter;

/**
 * 系统角色查询参数
 * Created by binchao on 2017/4/6.
 */
public class RoleQueryParameter extends QueryParameter {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
