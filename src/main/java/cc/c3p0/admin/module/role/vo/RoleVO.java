package cc.c3p0.admin.module.role.vo;

import javax.validation.constraints.NotNull;

/**
 * 系统角色VO
 * Created by binchao on 2017/4/8.
 */
public class RoleVO {

    @NotNull(message = "角色名称不能为空！")
    private String name;

    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
