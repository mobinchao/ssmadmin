package cc.c3p0.admin.module.role.service.impl;

import cc.c3p0.admin.common.mapper.BasicMapper;
import cc.c3p0.admin.common.service.impl.BasicServiceImpl;
import cc.c3p0.admin.mybatis.mapper.AdminRoleMapper;
import cc.c3p0.admin.mybatis.model.AdminRole;
import cc.c3p0.admin.module.role.service.RoleServiceI;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 系统角色管理业务实现
 * Created by binchao on 2017/4/6.
 */
@Service
public class RoleServiceImpl extends BasicServiceImpl<AdminRole>
        implements RoleServiceI {

    @Resource
    private AdminRoleMapper roleMapper;

    @Override
    protected BasicMapper<AdminRole> getMapper() {
        return roleMapper;
    }

    @Override
    public void grantMenus(String id, String menuIds) {
        AdminRole role = new AdminRole();
        role.setId(id);
        role.setMids(menuIds);
        roleMapper.updateByPrimaryKeySelective(role);
    }

    @Override
    public void grantResources(String id, String resIds) {
        AdminRole role = new AdminRole();
        role.setId(id);
        role.setRids(resIds);
        roleMapper.updateByPrimaryKeySelective(role);
    }
}


