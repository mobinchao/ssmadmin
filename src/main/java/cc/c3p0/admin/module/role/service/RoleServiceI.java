package cc.c3p0.admin.module.role.service;


import cc.c3p0.admin.common.service.BasicServiceI;
import cc.c3p0.admin.mybatis.model.AdminRole;
/**
 * 系统角色管理业务接口
 * Created by binchao on 2017/4/6.
 */
public interface RoleServiceI extends BasicServiceI<AdminRole> {

    /**
     * 授权菜单给角色
     * @param id       角色ID
     * @param menuIds  菜单ID集合
     */
    void grantMenus(String id, String menuIds);

    /**
     * 授权资源给角色
     * @param id       角色ID
     * @param resIds   资源ID集合
     */
    void grantResources(String id, String resIds);
}
