package cc.c3p0.admin.module.resouce.vo;

import cc.c3p0.admin.mybatis.model.AdminResource;

/**
 * 资源VO
 * Created by binchao on 2017/4/21.
 */
public class ResourceVO extends AdminResource {

    private String sname;

    private String mname;

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }
}
