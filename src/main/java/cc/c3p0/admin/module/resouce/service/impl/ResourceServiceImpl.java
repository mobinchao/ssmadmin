package cc.c3p0.admin.module.resouce.service.impl;

import cc.c3p0.admin.common.dto.Tree;
import cc.c3p0.admin.common.mapper.BasicMapper;
import cc.c3p0.admin.common.service.impl.BasicServiceImpl;
import cc.c3p0.admin.mybatis.mapper.AdminResourceMapper;
import cc.c3p0.admin.mybatis.model.AdminResource;
import cc.c3p0.admin.module.resouce.service.ResourceServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

/**
 * 系统资源管理业务实现
 * Created by binchao on 2017/4/8.
 */
@Service
public class ResourceServiceImpl extends
        BasicServiceImpl<AdminResource> implements
        ResourceServiceI {

    @Resource
    private AdminResourceMapper resourceMapper;

    @Override
    protected BasicMapper<AdminResource> getMapper() {
        return resourceMapper;
    }

    @Override
    public List<Tree> getTree() {
        List<Tree> tree = resourceMapper.getSystemIds();
        return tree;
    }
}
