package cc.c3p0.admin.module.resouce.service;

import cc.c3p0.admin.common.dto.Tree;
import cc.c3p0.admin.common.service.BasicServiceI;
import cc.c3p0.admin.mybatis.model.AdminResource;

import java.util.List;

/**
 * 系统资源管理业务接口
 * Created by binchao on 2017/4/8.
 */
public interface ResourceServiceI extends
        BasicServiceI<AdminResource> {

    /**
     * 获取资源树
     * @return
     */
    List<Tree> getTree();
}
