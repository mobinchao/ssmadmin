package cc.c3p0.admin.module.resouce.dto;

import cc.c3p0.admin.common.dto.QueryParameter;

/**
 * 资源查询参数BEAN
 * Created by binchao on 2017/4/13.
 */
public class ResourceQueryParameter extends QueryParameter {

    private String sid;

    private String mid;

    private String name;

    private String url;

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
