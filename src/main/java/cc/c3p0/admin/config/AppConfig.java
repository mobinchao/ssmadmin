package cc.c3p0.admin.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
/**
 * 项目配置
 * @author binchao
 * 2016年10月24日
 */
@Configuration
@ImportResource({"classpath:spring.xml"})
public class AppConfig {

}
