package cc.c3p0.admin.config.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;

/**
 * Druid Servlet Config
 * @author binchao
 * 2016年10月24日
 */
@Configuration
public class DruidWebStatServlet {
	
	private static Logger LOG = LoggerFactory.getLogger(DruidWebStatServlet.class);

	@Bean
	public ServletRegistrationBean druidServlet() {
		LOG.info("正在配置Druid Servlet...");
		ServletRegistrationBean reg = new ServletRegistrationBean();
		reg.setServlet(new StatViewServlet());
		reg.addUrlMappings("/druid/*");
		//reg.addInitParameter("allow", "127.0.0.1");
        //reg.addInitParameter("deny","");
        reg.addInitParameter("loginUsername", "admin");
        reg.addInitParameter("loginPassword", "admin");
        LOG.info("Druid Servlet配置完成...");
		return reg;
	}

	public FilterRegistrationBean filterRegistrationBean() {
		FilterRegistrationBean filter = new FilterRegistrationBean();
		filter.setFilter(new WebStatFilter());
		filter.addUrlPatterns("/*");
		filter.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
		return filter;
	}
}
