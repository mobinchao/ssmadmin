package cc.c3p0.admin.controller;

import cc.c3p0.admin.ControllerTest;
import cc.c3p0.admin.common.dto.Json;
import cc.c3p0.admin.mybatis.model.AdminUser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by binchao on 2017/3/3.
 */
public class UserControllerTest extends ControllerTest {

    private static final Logger LOG = LoggerFactory.getLogger(UserControllerTest.class);

    @Test
    public void saveUser() throws Exception {
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/user/register")
                .param("username", "admin")
                .param("password", "admin")
                .param("name", "超级管理员")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
//                .andDo(print())
//                .andExpect(content().string(equalTo("{\"success\":true,\"code\":0,\"msg\":\"注册成功！\",\"obj\":null}")))
                .andExpect(content().string(containsString("注册成功！")))
                .andReturn();
        MockHttpServletResponse resp = result.getResponse();
        LOG.info(resp.getContentAsString());
        ObjectMapper mapper = new ObjectMapper();
        Json json = mapper.readValue(resp.getContentAsString(), Json.class);
        assertThat(json.getMsg()).isEqualTo("注册成功！");
        assertThat(json.isSuccess()).isEqualTo(true);
    }

    @Test
    public void getUser() throws Exception {
        String id = "810f0d4a979c46fb99e3f2a5464aa33b";
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/user/get")
                .param("id", id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        ObjectMapper mapper = new ObjectMapper();
        Json json = mapper.readValue(result.getResponse().getContentAsString(), Json.class);
        Map<String, Object> map = (Map<String, Object>) json.getObj();
//        assertThat(map.get("username")).isEqualTo("admin111");  // assert fail
        assertThat(map.get("username")).isEqualTo("admin");    //  assert success
    }

    @Test
    public void login() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/user/login")
                .param("username", "admin")
                .param("password", "admin"))
                .andExpect(status().isOk())
                .andDo(print());
    }

}
