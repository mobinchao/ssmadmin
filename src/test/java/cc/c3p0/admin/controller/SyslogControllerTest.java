package cc.c3p0.admin.controller;

import cc.c3p0.admin.ControllerTest;
import cc.c3p0.admin.common.dto.Json;
import cc.c3p0.admin.module.syslog.dto.SyslogQueryParameter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by binchao on 2017/4/10.
 */
public class SyslogControllerTest extends ControllerTest {

    private static final Logger LOG = LoggerFactory.getLogger(SyslogControllerTest.class);

    @Test
    public void testGet() throws Exception {
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/syslog/get")
                .param("id", "11111")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        MockHttpServletResponse resp = result.getResponse();
        LOG.info(resp.getContentAsString());
        ObjectMapper mapper = new ObjectMapper();
        Json json = mapper.readValue(resp.getContentAsString(), Json.class);
        assertThat(json.isSuccess()).isEqualTo(true);
    }

    @Test
    public void testFind() throws Exception {
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/syslog/find")
                .param("pageSize","20")
                .param("pageNum", "2")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        MockHttpServletResponse resp = result.getResponse();
        LOG.info(resp.getContentAsString());
        ObjectMapper mapper = new ObjectMapper();
        Json json = mapper.readValue(resp.getContentAsString(), Json.class);
        assertThat(json.isSuccess()).isEqualTo(true);
    }
}
