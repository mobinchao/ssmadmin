package cc.c3p0.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;

/**
 * 通用控制器测试类
 * Created by binchao on 2017/3/3.
 */
@AutoConfigureMockMvc
public class ControllerTest extends AdminApplicationTests {

    @Autowired
    protected MockMvc mvc;
}
