package cc.c3p0.admin.service;

import cc.c3p0.admin.AdminApplicationTests;
import cc.c3p0.admin.module.group.dto.GroupQueryParameter;
import cc.c3p0.admin.module.group.service.GroupServiceI;
import cc.c3p0.admin.module.user.dto.UserQueryParameter;
import cc.c3p0.admin.mybatis.model.AdminGroup;
import cc.c3p0.admin.mybatis.model.AdminUser;
import cc.c3p0.admin.module.user.service.UserServiceI;
import cc.c3p0.admin.module.user.vo.UserVO;
import com.github.pagehelper.Page;
import com.sun.deploy.util.StringUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * 用户管理业务测试
 * Created by binchao on 2017/3/6.
 */
public class UserServiceTest extends AdminApplicationTests {

    private static final Logger LOG = LoggerFactory.getLogger(UserServiceTest.class);

    @Resource
    private UserServiceI userService;

    @Resource
    private GroupServiceI groupService;

    @Test
    public void save() {
        UserVO vo = new UserVO();
        vo.setUsername("admin");
        vo.setPassword("admin");
        userService.saveUser(vo);
    }

    @Test
    public void getUser() {
        String id = "810f0d4a979c46fb99e3f2a5464aa33b";
        AdminUser user = userService.select(id);
        assertThat(user.getUsername()).isEqualTo("admin");
    }

    @Test
    public void grantGroup() {
        UserQueryParameter param = new UserQueryParameter();
        param.setUsername("admin");
        Page<AdminUser> userList = userService.find(param);
        LOG.info("当前用户数：" + userList.size());
        if (userList.size() == 1) {
            List<AdminGroup> groupList = groupService.find(new GroupQueryParameter());
            LOG.info("当前分组数：" + groupList.size());
            if (groupList.size() > 0) {
                StringBuilder sb = new StringBuilder();
                for (AdminGroup g : groupList) {
                    sb.append(g.getId()).append(",");
                }
                AdminUser user = userList.get(0);
                user.setGids(sb.toString());
                userService.update(user);
            }
        }
    }

    @Test
    public void testCount() {
        int count = userService.count(null);
        assertThat(count).isEqualTo(3);
    }
}
