package cc.c3p0.admin.service;

import cc.c3p0.admin.AdminApplicationTests;
import cc.c3p0.admin.common.dto.Tree;
import cc.c3p0.admin.module.system.service.SystemServiceI;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by binchao on 2017/6/15.
 */
public class SystemServiceTest extends AdminApplicationTests {

    private static final Logger LOG = LoggerFactory.getLogger(SystemServiceTest.class);

    @Resource
    private SystemServiceI systemService;

    @Test
    public void testGetSystemTree() {
        String id = "f043a1edddc5477bbf82cd3261778cd5";
        List<Tree> tree = systemService.getSystemTree(id);
        LOG.info("info", tree);
    }
}
