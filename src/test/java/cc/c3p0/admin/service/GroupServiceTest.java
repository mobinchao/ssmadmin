package cc.c3p0.admin.service;

import cc.c3p0.admin.AdminApplicationTests;
import cc.c3p0.admin.common.dto.Tree;
import cc.c3p0.admin.module.group.dto.GroupQueryParameter;
import cc.c3p0.admin.module.group.service.GroupServiceI;
import cc.c3p0.admin.mybatis.model.AdminGroup;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Created by binchao on 2017/4/7.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GroupServiceTest extends AdminApplicationTests {

    @Autowired
    private GroupServiceI groupService;

    private static String id = UUID.randomUUID().toString().replace("-", "");


    @Test
    public void test1Save() {
        AdminGroup group = new AdminGroup();
        group.setId(id);
        group.setName("超级管理员");
        group.setDescription("拥有系统所有权限组");
        int i = groupService.save(group);
        assertThat(i).isEqualTo(1);
    }

    @Test
    public void test2Update() {
      AdminGroup group = new AdminGroup();
      group.setId(id);
      group.setName("测试管理员");
      int i = groupService.update(group);
      assertThat(i).isEqualTo(1);
    }

    @Test
    public void test3Get() {
        AdminGroup group = groupService.select(id);
        assertThat(group.getName()).isEqualTo("测试管理员");
    }

    @Test
    public void test4Find() {
        GroupQueryParameter param = new GroupQueryParameter();
        List<AdminGroup> list = groupService.find(param);
        assertThat(list.size()).isEqualTo(1);
    }

    @Test
    public void test5Delete() {
        int i = groupService.delete(id);
        assertThat(i).isEqualTo(1);
    }

    @Test
    public void testGroupTree() {
        String id = "5db64521931f40e3bd68abf943561f5a";
        List<Tree> list = groupService.getGroupTree(id);
        assertThat(list.size()).isEqualTo(1);
    }
}
