#ssmadmin 
一款用spring boot + mybatis + jwt开发的后台管理api，适合前后分离的项目使用
可与本人另一前端项目完美结合：[iviewadmin](http://git.oschina.net/mobinchao/iviewadmin)

# 技术选型
![技术选型](doc/screen/pic.png)

# 代码生成
  1. unix
  
    ./gen.sh

# 打包

  1. unix
  
    build-{profile}.sh
    
  2. windows
  
    build-{profile}.bat
    
#说明
1. 本项目开启CORS跨域资源共享协议
2. 前后端交互全程使用json格式
3. 认证模块使用jwt，使这套API更适合分布式环境。

# 截图
![登录](doc/screen/login.png)
![获取用户信息](doc/screen/user_get.png)
![获取系统菜单信息](doc/screen/menu_tree.png)